# Automatic Battery Mode Switch 
# 
# This script is to switch b/w Conservation and Normal Mode at 80% for supported Lenovo laptops.

$iFull = (Get-WmiObject -Namespace "root\wmi" -Query "select * from BatteryFullChargedCapacity").FullChargedCapacity
$batteryModePath = "HKCU:\Software\Lenovo\VantageService\AddinData\IdeaNotebookAddin"

function RestartLenovoToolkit {
    try {
        $process = Get-Process "Lenovo Legion Toolkit"
    } catch {
        $process = $null
    }
    if ($null -eq $process) {
        return
    } else {
        $processPath = $process.Path
        $processID = $process.Id
        Stop-Process -Id $processID
        Start-Sleep -Seconds 5
        Start-Process $processPath
    }

}


while (1){
    $currentMode = Get-ItemPropertyValue -Path $batteryModePath -Name "BatteryChargeMode"
    $batteryStatus = Get-WmiObject -Namespace "root\wmi" -Query "select * from BatteryStatus"
    $iRemaining = $batteryStatus.RemainingCapacity
    $iPercent = [Math]::Floor((($iRemaining / $iFull) * 100) % 100)    
    if ((($currentMode -eq "Normal") -or ($currentMode -eq "Quick")) -and ($iPercent -gt 79)){
        msg.exe * /time:5 'Battery more than 79%, switching to Conservation Mode.'
        Set-ItemProperty -Path $batteryModePath -Name "BatteryChargeMode" -Value "Storage" -Force
        RestartLenovoToolkit
   }
    if (($currentMode -eq "Storage") -and ($iPercent -lt 80)){
        msg.exe * /time:5 'Battery less than 80%, switching to Normal Mode.'
        Set-ItemProperty -Path $batteryModePath -Name "BatteryChargeMode" -Value "Normal" -Force
        RestartLenovoToolkit
    }
    Start-Sleep -Seconds 150
}
