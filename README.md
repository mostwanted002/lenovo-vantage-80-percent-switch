# lenovo-vantage-80-percent-switch

## Installation
 
1. Copy the `batterySwitch.ps1` file to your desired location.
2. Go to Windows Task Scheduler
3. Create a new task.
4. On "General" tab under Security Options, Select "Run only when user is logged on" and check "Run with highest privileges"
5. On "Triggers" tab, select New, and "At Log On", configure it to run when you (your user) logs in.
6. On "Actions" tab, Start a Program, for Program/Script, use `C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe`. For Arguments: `-WindowStyle Hidden -ExecutionPolicy Bypass -File "<PATH_TO_YOUR_batterySwitch.ps1_FILE>"`
7. Under "Conditions", uncheck everything under "Power" section.
8. Under "Settings", check the following: 
    - Allow task to be run on demand
    - If the task fails, restart every 5 minutes for maximum of 5 times
    - If the running task doesn't end when requested, force it stop
    - If the task is already running, then the following rule applies : Stop the existing instance
9. Click Ok. Done.
